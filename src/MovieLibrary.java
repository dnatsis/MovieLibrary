import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
 
public class MovieLibrary {
	private ArrayList<String> MovieList;
	private Lock MovieChangeLock;
	private Condition movieExistsCondition;
		
	public MovieLibrary(){
		MovieList = new ArrayList<String>();
		MovieChangeLock = new ReentrantLock();
		movieExistsCondition = MovieChangeLock.newCondition();
	}
	
	public void AddMovie(String movie){
		MovieChangeLock.lock();
		try {
		System.out.print(" Adding the Movie:" + movie + "\n");
		MovieList.add(movie);
		movieExistsCondition.signalAll();
		}
		finally {
		     MovieChangeLock.unlock();
		}
	}
		
	public void RemoveMovie(String movie){
		MovieChangeLock.lock();
		try {
			if (movieFind(movie) == true){
				System.out.print(" Removing the Movie:" + movie + "\n");
				MovieList.remove(movie);	
			}
			else {
				System.out.print(movie + " not found! \n");
			}
		}
		finally {
			MovieChangeLock.unlock();
		    }
		
	}
	
	public boolean movieFind(String search){
		boolean result = false;
		int searchListLength = MovieList.size();
		for (int i = 0; i < searchListLength; i++) {
			if (MovieList.get(i).contains(search)) {
			result = true;
			}
		else {
			result = false;
		}
		}
		return result;
	}

	public ArrayList<String> getLibrary(){
		return MovieList;
	}
}
