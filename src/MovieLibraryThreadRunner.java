import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MovieLibraryThreadRunner {
	public static void main(String args[]) throws FileNotFoundException {
		MovieLibrary movielibrary = new MovieLibrary();
		String movie;
		File MovieLibraryTXTfile = new File("movieLibrary.txt");
		Scanner sc = new Scanner(MovieLibraryTXTfile);
		final int REPETITIONS = 10;
		final int THREADS = 10;
		
	while (sc.hasNextLine()){
		movie = sc.nextLine();
		for (int i = 1; i <= THREADS; i++) {
			MovieAddRunnable a = new MovieAddRunnable(movielibrary,movie,REPETITIONS);
			MovieRemoveRunnable b = new MovieRemoveRunnable(movielibrary,movie,REPETITIONS);
			Thread dt = new Thread(a);
			Thread wt = new Thread(b);
			dt.start();
			wt.start();
		}
		 
	}
	sc.close();
	}
}
