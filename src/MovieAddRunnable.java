public class MovieAddRunnable implements Runnable {
	
	private static final int DELAY = 3000;
	private MovieLibrary movielibrary;
	private String movie;
	private int count;
	  	  
	public MovieAddRunnable(MovieLibrary lib, String mov, int aCount) {
		movielibrary = lib;
		movie = mov;
		count =aCount;
	}
	   
	public void run() {
		try {
			for (int i = 1; i <= count; i++) {
				movielibrary.AddMovie(movie);
				Thread.sleep(DELAY);
			}
	    }
		catch (InterruptedException e) {
			
		}
	}


}
